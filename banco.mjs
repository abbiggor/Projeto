import { question } from "readline-sync"

export let funcionarios =[];

/////////// ==== CADASTRO DO NOME ==== ///////////
export function cadastrarFunc() {
        let nomeFunc = question('Nome do funcionário3: ')
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////// ====== CADASTRO DO SETOR ====== ///////
        let setorFunc = 'nenhuma';
        let opcaoValidaSect = false;
        let opcaoCadSect;

        while(!opcaoValidaSect) {
            opcaoCadSect = parseInt(question( 'Para qual setor o funcionário será contratado?' + 
                                                '\n' + '1) Financeiro' +
                                                '\n' + '2) Fiscal' +
                                                '\n' + '3) Contábil' + 
                                                '\n' + 'Digite a opção correspondente: '
                                            )
                                    );
            opcaoValidaSect = true;
            switch (opcaoCadSect) {
                case 1:
                    setorFunc = 'Financeiro';
                    break;
                case 2:
                    setorFunc = 'Fiscal';
                    break;
                case 3: 
                    setorFunc = 'Contábil';
                    break;
                default:
                    console.log('Opção inválida');
                    opcaoValidaSect = false;
                    break;
            }
            console.log(`${nomeFunc} será contratado para o setor ${setorFunc}.
            `)

        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////// ======= CADASTRO DE FUNÇÃO =======/////
        let funcaoFunc = 'nenhuma';
        let opcaoValida = false;
        let opcaoCadFunct;

        while(!opcaoValida) {
            opcaoCadFunct = parseInt(question( 'Cadastre a função' + 
                                                '\n' + '1) Auxiliar' +
                                                '\n' + '2) Analista' +
                                                '\n' + '3) Líder' + 
                                                '\n' + 'Digite a opção correspondente: '
                                            )
                                    );
            opcaoValida = true;
            switch (opcaoCadFunct) {
                case 1:
                    funcaoFunc = 'Auxiliar';
                    break;
                case 2:
                    funcaoFunc = 'Analista';
                    break;
                case 3: 
                    funcaoFunc = 'Líder';
                    break;  
                default:
                    console.log('Opção inválida');
                    opcaoValida = false;
                    break;
            }
            console.log(`${nomeFunc} será contratado como ${funcaoFunc} no setor ${setorFunc}.
            `)


        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        let dataAdmFunc = question('Data de admissão: ')
        let salarioFunc = question('Salário: ')


        console.log('------------------ CADASTRO REALIZADO COM SUCESSO -----------------------')
        console.log(`O funcionário ${nomeFunc} foi contratado em ${dataAdmFunc} para a função de ${funcaoFunc}`)
        console.log(`no setor ${setorFunc}. ${nomeFunc} receberá mensalmente a importância de ${salarioFunc}.`)
        console.log('==========================================================================')

        funcionarios.push(
            {
                nome: nomeFunc,
                funcao: funcaoFunc,
                setor: setorFunc,
                data: dataAdmFunc,
                salario: salarioFunc
            }
        );
    }
    /////////////// ======== LISTAGEM ======== ////////////

    export function listarFunc() {
        console.log('Liste os funcionários.')
        console.table(funcionarios);
    }
    ///////////////////////===== PESQUISA POR FUNÇÃO =====////////////////////////////////////
    export function buscarFuncPorFuncao() {
        let funcFuncao = question('Pesquisa por função: ')
         
         for (let i = 0; i < funcionarios.length; i++) {
             const funcionario = funcionarios[i];
             if(funcionario.setor == funcFuncao)
                 console.table(funcionario)            
         }
     }
     ///////////////////////////////////////////////////////////