//

import { question } from 'readline-sync'
import * as bd from "./banco.mjs"


let opcao ;

while (true) {
    console.clear();
    console.log(      
`
===== SISTEMA DE CADASTRO DE FUNCIONÁRIOS =====
1 - Cadastrar funcionário
2 - Listar funcionários
3 - Buscar funcionário por função
4 - Buscar funcionário por setor
5 - Sair
`
    
    );

    opcao = parseInt(question('Escolha uma opção: '));

    switch (opcao) {
        case 1:
            bd.cadastrarFunc();
            break;
        case 2:
            bd.listarFunc();
            break;
        case 3:
            bd.buscarFuncPorFuncao();
            break;
        case 4:
            buscarSetor();
            break;        
        case 5:
            console.log('Fim do programa');
            process.exit(0);
            break;
        default:
            console.log('Opção inválida');
            break;
    }
    question('Tecle ENTER para continuar....');
}
    